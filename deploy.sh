#!/bin/sh

ssh -o StrictHostKeyChecking=no ubuntu@$DEPLOY_SERVER << 'ENDSSH'
  cd /home/ubuntu
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:latest
  docker run --rm -it -p 80:80 $IMAGE:latest
ENDSSH
